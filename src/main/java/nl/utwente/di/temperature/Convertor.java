package nl.utwente.di.temperature;

public class Convertor {

    public double convertTemp(double celsius) {
        return (celsius * 9.0 / 5.0) + 32.0;
    }
}
