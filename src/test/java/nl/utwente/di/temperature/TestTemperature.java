package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTemperature {
    @Test
    public void testBook1() throws Exception {
        Convertor convertor = new Convertor();
        double price = convertor.convertTemp(0);
        Assertions.assertEquals(32.0, price);
    }
}
